﻿using System.Diagnostics;
using System.Numerics;

namespace ArraySum
{
    public class Summarize
    {

        public long ArraySum(long[] array)
        {
            long result = 0;
            foreach (var item in array)
            {
                result += item;
            }

            return result;

        }

        public async Task<List<Task<long>>> ArraySumAsync(long[] array, int threads = 4)
        {
            List<Task<long>> tasks = [];
            int count = array[0..((array.Length / threads) + 1)].Count();
            var chunk = array.Chunk(count);
            foreach (var item in chunk)
            {
                tasks.Add(new Task<long>(() =>
                {
                   return Sum(item);
                }));
            }

            foreach (var task in tasks)
            {
                task.Start();
            }
            return tasks;
        }

        private long Sum(long[] array) 
        {
            long result = 0;
            foreach (var item in array)
            {
                result += item;
            }
            return result;
        }

        public long ParallelArraySumAsync(long[] array)
        {
            return array.AsParallel().Sum();
        }

    }
}
