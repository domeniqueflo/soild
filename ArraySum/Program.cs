﻿using ArraySum;
using System.Diagnostics;

int[] array = Enumerable.Range(0, 10_000_000).ToArray();
long[] arr = array.Select(x => (long)x).ToArray();
Summarize summarize = new Summarize();
Stopwatch sw = new();
sw.Start();
var sum = summarize.ArraySum(arr);
Console.WriteLine(sum);
sw.Stop();
Console.WriteLine($"synchro summ: {sw.ElapsedMilliseconds}");
sw.Reset();

sw.Start();
var async = summarize.ArraySumAsync(arr);
Task.WaitAll(async);

long res = 0;
foreach (var item in async.Result)
{
    res += item.Result;
}
Console.WriteLine(res);
sw.Stop();
Console.WriteLine($"async summ: {sw.ElapsedMilliseconds}");
sw.Reset();

sw.Start();
var pl = summarize.ParallelArraySumAsync(arr);
Console.WriteLine(pl);
sw.Stop();
Console.WriteLine($"plinq summ: {sw.ElapsedMilliseconds}");
