﻿using Except;


internal class Program
{
    private static void Main(string[] args)
    {
        Publisher p = new Publisher();
        Subscriber subscriber = new Subscriber(1);
        Subscriber subscriber1 = new Subscriber(2);

        p.SetMsq += subscriber.OnDataReceved;
        p.SetMsq += subscriber1.OnDataReceved;
        p.Send("hello");


    }
}

internal class Publisher
{
    public delegate void EventHandler(string msq);
    public event EventHandler? SetMsq;

    public void Send(string msq)
    {
        SetMsq?.Invoke(msq);
    }
}

internal class Subscriber
{
    public int Id;
    public Subscriber(int id)
    {
        Id = id;
    }
    public void OnDataReceved(string msq)
    {
        Console.WriteLine($"{Id}:{msq}");
    }
}