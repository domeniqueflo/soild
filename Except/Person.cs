﻿namespace Except
{
    internal class Person
    {
        public string Name { get; set; }
        public int Hp { get; set; }
        public int Attack { get; set; }

        public void GetDamage(int damage)
        {
            Hp -= damage;
        }

    }
}
