﻿using ClonePattern.core.domain.entites;
using ClonePattern.core.application.astractions.service.abstractions;
using ClonePattern.core.application.application.services;

namespace ClonePattern.Tests.application.serivice.tests
{
    public class GetCloneObjectTests
    {
        private readonly IService<Engeneere> _service;

        public GetCloneObjectTests()
        {
            _service = new EngeneereService();
        }

        [Fact]
        public void Get_Valid_Clone_Object_For_Valid_Params()
        {
            //Act
            var engeneer = new Engeneere("Bob", "Martin", "Noob suppirt");
            //Arrange

            var clone = _service.Clone(engeneer);
            //Assert

            Assert.NotNull(clone);
            Assert.Equal(engeneer, clone.Last());
        }

        [Fact]
        public void Get_Error_For_Null_Params()
        {
            //Act
            Engeneere? engeneer = null;
            //Arrange

            var clone = _service.Clone(engeneer);
            //Assert

            Assert.Null(clone);
        }

    }
}
