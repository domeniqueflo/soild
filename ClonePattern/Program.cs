﻿using ClonePattern.core.application.application.services;
using ClonePattern.core.domain.entites;

namespace ClonePattern
{
    internal class Program
    {
        static void Main(string[] args)
        {

            var engeneereService = new EngeneereService();
            Engeneere[] cloneResult = engeneereService.Clone(new Engeneere("Bob", "Martin", "Noob Support")).ToArray();
            Console.WriteLine(cloneResult[0].Equals(cloneResult[1]));
        }

    }
}
