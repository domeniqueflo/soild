﻿using ClonePattern.core.application.astractions.service.abstractions;
using ClonePattern.core.domain.entites;

namespace ClonePattern.core.application.application.services
{
    public class EngeneereService : IService<Engeneere>
    {
        public ICollection<Engeneere> Clone(Engeneere item)
        {
            if(item is Engeneere == false)
            {
                return null;
            }
            return
            [
                item,
                item.CustomClone()
            ];
        }
    }
}