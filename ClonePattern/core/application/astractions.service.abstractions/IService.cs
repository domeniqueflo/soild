﻿using ClonePattern.core.domain.entites;

namespace ClonePattern.core.application.astractions.service.abstractions
{
    public interface IService<T> where T : Person
    {
        public ICollection<T> Clone(T item);
    }
}
