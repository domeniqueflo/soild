﻿namespace ClonePattern.core.domain.abstractions
{
    public interface IMyCloneable<T>
    {
        T CustomClone();
    }
}
