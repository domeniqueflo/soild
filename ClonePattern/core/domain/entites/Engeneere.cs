﻿using ClonePattern.core.domain.abstractions;

namespace ClonePattern.core.domain.entites
{
    public class Engeneere : Emploee, ICloneable, IMyCloneable<Engeneere>
    {
        public string Post { get; private set; }
        public Engeneere(string firstName, string lastName, string departament)
            : base(firstName, lastName, departament)
        {
            Post = "Engeneer";
        }

        public override bool Equals(object? obj)
        {
            if (obj is Engeneere)
            {
                var other = obj as Engeneere;
                if (other?.FirstName != this.FirstName ||
                    other?.LastName != this.LastName ||
                    other.Departament != this.Departament ||
                    other?.Post != this.Post
                )
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public override Engeneere CustomClone()
        {
            return new Engeneere(FirstName, LastName, Departament);
        }
        public override int GetHashCode()
        {
            return $"{this.FirstName}{this.LastName}{this.Departament}{this.Post}".GetHashCode();
        }
    }
}
