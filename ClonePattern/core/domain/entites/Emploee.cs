﻿using ClonePattern.core.domain.abstractions;

namespace ClonePattern.core.domain.entites
{
    public class Emploee : Person, ICloneable, IMyCloneable<Emploee>
    {
        public string Departament { get; private set; }
        public Emploee(string firstName, string lastName, string departament) : base(firstName, lastName)
        {
            Departament = departament;
        }

        public override bool Equals(object? obj)
        {
            if (obj is Emploee)
            {
                var other = obj as Emploee;
                if (other?.FirstName != this.FirstName || 
                    other?.LastName != this.LastName   ||
                    other.Departament != this.Departament
                )
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public override Emploee CustomClone()
        {
            return new Emploee(FirstName, LastName, Departament);
        }

        public override int GetHashCode()
        {
            return $"{this.FirstName}{this.LastName}{this.Departament}".GetHashCode();
        }
    }
}
