﻿using ClonePattern.core.domain.abstractions;

namespace ClonePattern.core.domain.entites
{
    public class Person : ICloneable, IMyCloneable<Person>
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public object Clone()
        {
            return (Person)MemberwiseClone();
        }

        public virtual Person CustomClone()
        {
            return new Person(FirstName, LastName);
        }

        public override bool Equals(object? obj)
        {
            if (obj is Person)
            {
                var other = obj as Person;
                if (other?.FirstName != this.FirstName || other?.LastName != this.LastName)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
           return $"{this.FirstName}{this.LastName}".GetHashCode();
        }
    }
}
