﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Async
{
    public class Runner
    {
        private readonly TaskFactory _taskFactory;
        public Runner()
        {
            _taskFactory = new TaskFactory();
        }
        public void ReadFile(string[] files)
        {
            Stopwatch watch = Stopwatch.StartNew(); 
            foreach (string file in files)
            {
                var path = Path.GetFullPath($"..\\..\\..\\source_data\\{file}");
                var task = _taskFactory.StartNew(() => FileLoader(path));
                task.Wait();
            }
            watch.Stop();
            Console.WriteLine($"done of {watch.ElapsedMilliseconds} ms");
        }
        private void FileLoader(string pathToFile)
        {
            int space = 0;
            var file = File.ReadAllText(pathToFile);
            foreach (var sym in file)
            {
                if (sym == ' ')
                    space++;
            }
            Console.WriteLine($"{pathToFile}: {space} space");
        }

        public void CalculateFile(string directory)
        {
            var files = Directory.GetFiles(directory, "*.*");
            int fileCount = 0;
            int totalFileSpace = 0;

            Stopwatch watch = Stopwatch.StartNew();
            var calculateFileTask = _taskFactory.StartNew(() => 
            {
                foreach (var file in files)
                {
                    fileCount++;
                }
            });

            var calculateTotlaSpaceTask = _taskFactory.StartNew(() =>
            {
                foreach (var file in files)
                {
      
                    var data = File.ReadAllText(file);
                    foreach (var sym in data)
                    {
                        if (sym == ' ')
                            totalFileSpace++;
                    }
                }
                
            });

            calculateFileTask.Wait();
            calculateTotlaSpaceTask.Wait();
            watch.Stop();
            Console.WriteLine($"done of {watch.ElapsedMilliseconds} ms");
            Console.WriteLine($"file count: {fileCount}");
            Console.WriteLine($"total space: {totalFileSpace}");
        }

        public void CalculateSpace(string directory)
        {
            var files = Directory.GetFiles(directory, "*.*");


            Stopwatch watch = Stopwatch.StartNew();
 
            foreach (var file in files)
            {
                var task = _taskFactory.StartNew(() => 
                {
                    int space = 0;
                    var data = File.ReadAllText(file);
                    foreach (var sym in data)
                    {
                        if (sym == ' ')
                            space++;
                    }
                    Console.WriteLine($"{file} space: {space}");
                });
                task.Wait();
            }

            watch.Stop();
            Console.WriteLine($"done of {watch.ElapsedMilliseconds} ms");
     
        }

    }
}
