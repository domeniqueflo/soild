﻿using Otus.Helpers;
using Otus.Interfaces;
using static System.Console;

namespace Otus
{
    public class Game
    {
        private ISettings _settings;
        private ISettingsConstructor _constructor;
        private IStepController _stepController;
        private int _number;
        public Game(ISettingsConstructor constructor, IStepController stepController)
        {
            _constructor = constructor;
            _stepController = stepController;

        }

        public Game Init()
        {
            _settings = _constructor.Init();
            return this;
        }

        public void Start()
        {
            Clear();
            while (true)
            {
                WriteLine("start new game  Y/N ?");
                var choise = ReadKey().Key;
                switch (choise)
                {
                    case ConsoleKey.Y:
                        Clear();
                        Run();
                        break;
                    case ConsoleKey.N:
                        Clear();
                        break;
                    default:
                        break;
                }
            }
        }


        public void Run()
        {
            _number = NumberGenerator.GetNumber(_settings, new Random());
            var counter = 0;

            WriteLine(_number);
            while (counter < _settings.StepCont)
            {
                WriteLine("enter number");
                var ansver = ReadLine();

                switch (Validator.IsValid(ansver))
                {
                    case false:
                        WriteLine("cymbol is not a number");
                        Thread.Sleep(1000);
                        Clear();
                        continue;
                }

                var stepResult = _stepController.GetStepResult(_number, int.Parse(ansver));
                Console.WriteLine(stepResult);
                switch (stepResult)
                {
                    case "you win":
                        return;
                }
                counter++;
            }
        }
    }
}
