﻿using Otus.Interfaces;
using System.Text.Json.Serialization;

namespace Otus
{
    public class Settings : ISettings
    {
        [JsonPropertyName("stepCont")]
        public int StepCont { get; set; }

        [JsonPropertyName("startPosition")]
        public int StartPosition { get; set; }

        [JsonPropertyName("endPosition")]
        public int EndPosition { get; set; }

    }
}
