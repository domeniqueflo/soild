﻿using Otus.Interfaces;

namespace Otus.Helpers
{
    public static class NumberGenerator
    {
        public static int GetNumber(ISettings settings, Random random) => random.Next(settings.StartPosition, settings.EndPosition);
    }
}
