﻿namespace Otus.Helpers
{
    public static class Validator
    {
        public static bool IsValid(string value)
        {
            return int.TryParse(value, out var result);
        }
    }
}
