﻿using Otus.Interfaces;

namespace Otus
{
    public class StepController : IStepController
    {
        public string GetStepResult(int number, int playerNumber)
        {
            string message = string.Empty;

            if (playerNumber > number)
            {
                message = "the number is lower";
            }
            else if (playerNumber < number)
            {
                message = "the number in high";
            }
            else if (playerNumber == number)
            {
                message = "you win";
            }
            return message;
        }
    }
}
