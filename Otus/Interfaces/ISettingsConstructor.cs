﻿namespace Otus.Interfaces
{
    public interface ISettingsConstructor
    {
        public ISettings Init();
    }
}
