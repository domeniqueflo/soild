﻿using System.Text.Json.Serialization;

namespace Otus.Interfaces
{
    public interface ISettings
    {
        public int StepCont { get; set; }
        public int StartPosition { get; set; }
        public int EndPosition { get; set; }
    }
}
