﻿namespace Otus.Interfaces
{
    public interface IStepController
    {
        public string GetStepResult(int number, int playerNumber);
    }
}
