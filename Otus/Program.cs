﻿using Otus;

var game = new Game(new SettingsConstructor(), new StepController());

game
    .Init()
    .Start();
 