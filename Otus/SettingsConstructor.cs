﻿using Otus.Interfaces;
using System.Text.Json;

namespace Otus
{
    public class SettingsConstructor : ISettingsConstructor
    {
        public ISettings Init()
        {
            using StreamReader reader = new(@"C:\Users\Anton\source\repos\Otus\Otus\settings.json");
            var json = reader.ReadToEnd();
            var setting = JsonSerializer.Deserialize<Settings>(json);
            return setting;
        }

    }
}
