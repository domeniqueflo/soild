﻿using System.Text;
using System.Reflection;

namespace Serializer
{
    public static class MainSerializer
    {
        public static string Serialize(object item)
        {
            var type = item.GetType();
            StringBuilder serializedData = new StringBuilder();

            foreach (var property in type.GetProperties())
            {
                serializedData.Append($"{property.Name} : {property.GetValue(item)} ");
            }

            return serializedData.ToString();
        }
        public static User Deserialize(string file)
        {
            var user = new User();
            using(var reader = new StreamReader(file))
            {
                var data = reader.ReadToEnd();
                
                data.Trim();
                var split = data.Split(",");

                var id = split[0].Split(":");
                var name = split[1].Split(":");
             
                var  email = split[2].Split(":");

                user.Id = int.Parse(id[1]);
                user.Name = name[1].Replace('"', ' ').Trim();
                user.Email = email[1].Replace('}', ' ').Replace('"', ' ').Trim();
            }
            return user;
        }
    }
}
