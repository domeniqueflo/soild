﻿using Serializer;
using System.Diagnostics;
using System.Text.Json;

var sw = new Stopwatch();
int counter = 0;

var user = new User
{
    Id = 1,
    Name = "Test",
    Email = "Test",
};

while (counter < 1000)
{
    sw.Start();

    var desirealizedUser = MainSerializer.Deserialize(@"C:\Users\Anton\source\repos\Otus\Serializer\data_1.ini");
    //var desirealizedUser = JsonSerializer.Deserialize<User>("data.ini");

    using (var reader = new StreamReader("data_1.ini"))
    {
        var data = reader.ReadToEnd();
        var u = JsonSerializer.Deserialize<User>(data);
        Console.WriteLine(u);
        Console.WriteLine(u.Id);
        Console.WriteLine(u.Name);
        Console.WriteLine(u.Email);
    }
    sw.Stop();
    Console.WriteLine(desirealizedUser);
    Console.WriteLine(desirealizedUser.Id);
    Console.WriteLine(desirealizedUser.Name);
    Console.WriteLine(desirealizedUser.Email);
    Console.WriteLine(sw.ElapsedMilliseconds);
    counter++;
}




/*using (var writer = new StreamWriter(@"C:\Users\Anton\source\repos\Otus\Serializer\data_1.ini", false))
{
    var us = JsonSerializer.Serialize(user);
    writer.WriteLine(us);
}*/