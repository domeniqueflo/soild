﻿namespace Attr
{
    [Flags]
    public enum Cars
    {
        First,
        Second,
        Third
    };

    public record Container (
        string Data
    );

   
    [VersionController("Author", 1)]
    public class DataContainer
    {
        public string Data { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class VersionController: Attribute
    {
        public string Name { get; }
        public string Date { get; }        
        public int Version { get; }

        public VersionController(string Name, int ver)
        {
            this.Name = "autor";
            Date = DateTime.Now.ToString();
        }

    }
}
