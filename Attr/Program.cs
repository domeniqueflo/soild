﻿using Attr;
using System.Reflection;

var verify = typeof(DataContainer).IsDefined(typeof(VersionController), false);
 if (verify)
{
    var author = typeof(DataContainer).GetCustomAttributes(typeof(VersionController), false).Single() as VersionController;
    Console.WriteLine(author.Name);
}
