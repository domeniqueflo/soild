﻿using Deleg;

class Program
{
    static void Main(string[] args)
    {
        Back();
        Get();
    }

    static void Back()
    {
        var scanner = new FileScanner();
        scanner.FileFound += Console.WriteLine;

        scanner.Run();
    }

    static void Get()
    {
     
        int[] array = { 1, 4, 12, 2, 44 };
        var res = Max<int>.GetMax(array, GM);
        Console.WriteLine(res);
    }

    private static float GM(IEnumerable<int> array)
    {
        return array.Max(x => x);
    }

}
