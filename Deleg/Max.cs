﻿namespace Deleg
{
    public static class Max<T>
    {

        public static float GetMax(IEnumerable<T> collection, Func<IEnumerable<T>, float>converter)
        {
            return converter(collection);
        }

    }
}
