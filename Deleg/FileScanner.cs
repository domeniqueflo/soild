﻿namespace Deleg
{
    public class FileScanner
    {
        public delegate void EventHandler(string msq);
        public EventHandler FileFound { get; set; }

        public void Run()
        {
            var currentDir = "C:\\";

            string[] subDir = Directory.GetDirectories(currentDir);

            foreach (string dir in subDir)
            {
                try
                {
                    string[] files = Directory.GetFiles(dir);
                    foreach (string file in files)
                    {
                        FileFound(file);
                    }
                }
                catch (Exception ex)
                {
                    FileFound(ex.Message);
                }
            }
        }

        public Task RunAsync()
        {
            return new Task(action: Run);
        }

    }
}
