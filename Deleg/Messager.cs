﻿namespace Deleg
{
    public class Messager
    {
        public delegate string Send(string msq);
        public Send ToSend { get; set; }

        public dynamic PrepareMessage()
        {
            var result =  new
            {
                message = ToSend("i")
            };

            return result;
        }
    }
}
