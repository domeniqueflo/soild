﻿using Parallel;
using System.Diagnostics;


var r = new Random();
var sw = new Stopwatch();


Thread[] pool =
[
    ThreadFactory.GetThread("email", DB.GetDb(r)),
    ThreadFactory.GetThread("sms", DB.GetDb(r))

];

Task[] tasks =
[
    ThreadFactory.GetTask("email", DB.GetDb(r)),
    ThreadFactory.GetTask("sms", DB.GetDb(r)),
];

sw.Start();
foreach (Thread thread in pool)
    thread.Join();

/*foreach (Task task in tasks)
    task.Wait();*/

    sw.Stop();
Console.WriteLine(sw.ElapsedMilliseconds);
