﻿namespace Parallel
{
    public record Message(string DateCreate, string MessageData, string SendTo);
}
