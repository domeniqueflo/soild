﻿namespace Parallel
{
    public static class ThreadFactory
    {
        public static Thread GetThread(string condition, List<Message> db)
        {
            var thread = new Thread(() =>
            {
                foreach (Message m in db.Where(i => i.SendTo == condition))
                {
                    Console.WriteLine(m);
                }
            });
            {
                //IsBackground = true
            };

            thread.Start();
            return thread;
        }

        public static Task GetTask(string condition, List<Message>db) 
        {
            var task = new Task(() =>
            {
                foreach (Message m in db.Where(i => i.SendTo == condition))
                {
                    Console.WriteLine(m);
                }
            });
            task.Start();
            return task;
        }
    }
}
