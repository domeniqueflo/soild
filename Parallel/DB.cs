﻿namespace Parallel
{
    public static class DB
    {
        public static List<Message> GetEmptyDb()
            => new List<Message>();

        public static List<Message> GetDb(Random r)
        {
            var db = new List<Message>();
            for (int i = 1; i < 1000; i++)
            {
                string sendTo = "sms";

                if ((i + r.Next(1, 9)) % 2 != 0)
                {
                    sendTo = "email";
                }



                db.Add(new Message("21/01/2024", $"{i} : {Guid.NewGuid()}", sendTo));
            }
            return db;
        }
    }
}
